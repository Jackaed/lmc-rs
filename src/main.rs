#![warn(clippy::cargo, clippy::pedantic, clippy::nursery)]
use clap::{Parser, ValueEnum};
use color_eyre::eyre::Result;

mod assembler;
mod interpreter;
mod state;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Arguments {
    #[arg(short = 'm', long)]
    operating_mode: OperatingMode,
    #[arg(short = 'i', long)]
    input_path: std::path::PathBuf,
    #[arg(short = 'o', long)]
    output_path: Option<std::path::PathBuf>,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum OperatingMode {
    Assemble,
    Interpret,
    Run,
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Arguments::parse();
    match args.operating_mode {
        OperatingMode::Assemble => {
            println!("{:?}", assembler::assemble(&args.input_path));
        }
        OperatingMode::Interpret => todo!(),
        OperatingMode::Run => todo!(),
    };
    Ok(())
}
