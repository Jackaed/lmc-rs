use bounded_integer::bounded_integer;

pub struct ProgramState {
    mailboxes: [Datum; 100],
    counter: Datum,
}

impl ProgramState {
    fn get(&self, index: MailboxIndex) -> &Datum {
        &self.mailboxes[std::convert::Into::<u8>::into(index) as usize]
    }

    fn get_mut(&mut self, index: MailboxIndex) -> &mut Datum {
        &mut self.mailboxes[std::convert::Into::<u8>::into(index) as usize]
    }

    fn set(&mut self, index: MailboxIndex, value: Datum) {
        *self.get_mut(index) = value;
    }
}

bounded_integer! {
    pub struct Datum {0..1000}
}

bounded_integer! {
    pub struct MailboxIndex {0..100}
}
