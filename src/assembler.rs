use std::{array, collections::HashMap, convert::TryInto, fs, path::PathBuf, slice::Iter};

use bounded_integer::bounded_integer;
use rayon::prelude::*;

use crate::state::Datum;

pub fn assemble(input_path: &PathBuf) -> [Datum; 100] {
    let string = fs::read_to_string(input_path).expect("Invalid input path");
    let lines: Vec<&str> = string.lines().collect();

    let instructions: Vec<ParsedLine> = lines
        .par_iter()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .filter(|s| s.chars().next().unwrap_or(' ') != '#')
        .map(|s| TryInto::<ParsedLine>::try_into(s).unwrap())
        .collect();

    let mut label_line_numbers: HashMap<String, Datum> = HashMap::new();

    for (i, instruction) in instructions.iter().enumerate() {
        if let Some(label) = instruction.label.clone() {
            label_line_numbers.insert(
                label,
                Datum::new(u16::try_from(i).expect("Too many instructions"))
                    .expect("Too many instructions"),
            );
        }
    }

    let binding = instructions
        .par_iter()
        .map(|instruction| parse_line_label(instruction.clone(), &label_line_numbers))
        .collect::<Vec<_>>();

    let mut processed_instructions: Iter<_> = binding.iter();

    array::from_fn(|_| {
        processed_instructions
            .next()
            .map_or_else(|| Datum::new(0).unwrap(), |s| (*s).clone().into())
    })
}

fn parse_line_label(
    instruction: ParsedLine,
    label_line_numbers: &HashMap<String, Datum>,
) -> InstructionWithParsedLabels {
    match instruction.instruction {
        Instruction::WithOperand(instruction_with_operand, operand) => {
            InstructionWithParsedLabels::WithOperand(
                instruction_with_operand,
                match operand {
                    Operand::Label(label) => {
                        *label_line_numbers.get(&label).expect("Unknown label")
                    }
                    Operand::Value(value) => value,
                },
            )
        }
        Instruction::WithoutOperand(instruction_without_operand) => {
            InstructionWithParsedLabels::WithoutOperand(instruction_without_operand)
        }
    }
}

bounded_integer! {
    struct MailboxIndex {0..100}
}

#[derive(Debug, Clone)]
struct ParsedLine {
    instruction: Instruction,
    label: Option<String>,
}

#[derive(Debug, Clone)]
enum Operand {
    Label(String),
    Value(Datum),
}

#[derive(Debug, Clone)]
enum Instruction {
    WithOperand(InstructionWithOperand, Operand),
    WithoutOperand(InstructionWithoutOperand),
}

#[derive(Debug, Clone)]
enum InstructionWithParsedLabels {
    WithOperand(InstructionWithOperand, Datum),
    WithoutOperand(InstructionWithoutOperand),
}

/// This unwrap cannot fail.
#[allow(clippy::fallible_impl_from)]
impl From<InstructionWithParsedLabels> for Datum {
    fn from(instruction: InstructionWithParsedLabels) -> Self {
        match instruction {
            InstructionWithParsedLabels::WithOperand(instruction, operand) => {
                std::convert::Into::<Self>::into(instruction)
                    + Self::new(std::convert::Into::<u16>::into(operand)).unwrap()
            }
            InstructionWithParsedLabels::WithoutOperand(instruction) => instruction.into(),
        }
    }
}

/// These unwraps cannot fail. Ideally, this would be done with a macro instead
/// of at runtime, but my hand has been forced.
#[allow(clippy::fallible_impl_from)]
impl From<InstructionWithOperand> for Datum {
    fn from(instruction: InstructionWithOperand) -> Self {
        match instruction {
            InstructionWithOperand::Add => Self::new(100).unwrap(),
            InstructionWithOperand::Subtract => Self::new(200).unwrap(),
            InstructionWithOperand::Store => Self::new(300).unwrap(),
            InstructionWithOperand::Load => Self::new(500).unwrap(),
            InstructionWithOperand::Branch => Self::new(600).unwrap(),
            InstructionWithOperand::BranchOnZero => Self::new(700).unwrap(),
            InstructionWithOperand::BranchOnPositive => Self::new(800).unwrap(),
            InstructionWithOperand::Data => Self::new(0).unwrap(),
        }
    }
}

/// These unwraps cannot fail. Ideally, this would be done with a macro instead
/// of at runtime, but my hand has been forced.
#[allow(clippy::fallible_impl_from)]
impl From<InstructionWithoutOperand> for Datum {
    fn from(instruction: InstructionWithoutOperand) -> Self {
        match instruction {
            InstructionWithoutOperand::Input => Self::new(901).unwrap(),
            InstructionWithoutOperand::Output => Self::new(902).unwrap(),
            InstructionWithoutOperand::Halt => Self::new(0).unwrap(),
        }
    }
}

#[derive(Debug)]
struct InvalidAssemblyLine;

impl TryFrom<&str> for ParsedLine {
    type Error = InvalidAssemblyLine;

    fn try_from(from: &str) -> Result<Self, Self::Error> {
        let tokens: Vec<&str> = from
            .split_whitespace()
            .filter(|s| !s.is_empty())
            .map(str::trim)
            .collect();

        InstructionWithoutOperand::try_from(*tokens.last().ok_or(Self::Error {})?).map_or_else(
            |_| Self::calculate_fields_for_instruction_with_operand(&tokens),
            |instruction| {
                Self::calculate_fields_for_instruction_without_operand(&tokens, instruction)
            },
        )
    }
}

impl ParsedLine {
    fn calculate_fields_for_instruction_without_operand(
        tokens: &Vec<&str>,
        instruction: InstructionWithoutOperand,
    ) -> Result<Self, InvalidAssemblyLine> {
        let label: Option<String> = if tokens.len() == 2 {
            Some((*tokens.first().ok_or(InvalidAssemblyLine {})?).to_string())
        } else {
            None
        };

        Ok(Self {
            instruction: Instruction::WithoutOperand(instruction),
            label,
        })
    }

    fn calculate_fields_for_instruction_with_operand(
        tokens: &Vec<&str>,
    ) -> Result<Self, InvalidAssemblyLine> {
        let last_token = *tokens.last().ok_or(InvalidAssemblyLine {})?;
        let operand = last_token.parse::<Datum>().map_or_else(
            |_| Operand::Label((*last_token).to_string()),
            Operand::Value,
        );
        let label: Option<String> = if tokens.len() == 3 {
            Some(tokens[0].to_string())
        } else {
            None
        };
        Ok(Self {
            instruction: Instruction::WithOperand(
                InstructionWithOperand::try_from(
                    *tokens.get(tokens.len() - 2).ok_or(InvalidAssemblyLine {})?,
                )?,
                operand,
            ),
            label,
        })
    }
}

#[derive(Debug, Clone)]
enum InstructionWithOperand {
    Add,
    Subtract,
    Store,
    Load,
    Branch,
    BranchOnZero,
    BranchOnPositive,
    Data,
}

impl TryFrom<&str> for InstructionWithOperand {
    type Error = InvalidAssemblyLine;

    fn try_from(from: &str) -> Result<Self, Self::Error> {
        match from {
            "ADD" => Ok(Self::Add),
            "SUB" => Ok(Self::Subtract),
            "STO" => Ok(Self::Store),
            "LDA" => Ok(Self::Load),
            "BR" => Ok(Self::Branch),
            "BRZ" => Ok(Self::BranchOnZero),
            "BRP" => Ok(Self::BranchOnPositive),
            "DAT" => Ok(Self::Data),
            _ => Err(Self::Error {}),
        }
    }
}

#[derive(Debug, Clone)]
enum InstructionWithoutOperand {
    Input,
    Output,
    Halt,
}

impl TryFrom<&str> for InstructionWithoutOperand {
    type Error = InvalidAssemblyLine;
    fn try_from(from: &str) -> Result<Self, Self::Error> {
        match from {
            "IN" => Ok(Self::Input),
            "OUT" => Ok(Self::Output),
            "HLT" => Ok(Self::Halt),
            _ => Err(Self::Error {}),
        }
    }
}
